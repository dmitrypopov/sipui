var gulp = require('gulp'),
    jade = require('gulp-jade'),
    watch = require('gulp-watch'),
    babel = require('gulp-babel');

gulp.task('templates', function () {
    var YOUR_LOCALS = {};

    gulp.src('./*.jade')
        .pipe(jade({
            locals: YOUR_LOCALS
        }))
        .pipe(gulp.dest('./build/'))
});

gulp.task('scripts', function () {
    return gulp.src('./build/app/src/./*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('./build/app/'));
});

gulp.task('default', ['templates'], function () {
    gulp.watch(['./*.jade'], function () {
        gulp.run('templates');
    });
    gulp.watch(['./build/app/src/./*.js'], function () {
        gulp.run('scripts');
    });

});