'use strict';

var app = angular.module('sip', ['ui.mask']);
app.controller('Actions', function ($scope, $http) {
    $scope.inp1 = "100";
    $scope.inp2 = "100,204";
    $scope.inp3 = "100,204";
    $scope.select1 = "100";
    $scope.onClickBtn = function (buttonId, arg) {
        var parameter = {};
        switch (buttonId) {
            case 1:
                parameter = JSON.stringify({ cmd: "m", args: arg });
                break;
            case 2:
                parameter = JSON.stringify({ cmd: "mc", args: arg.match(/(\d+)/ig) });
                break;
            case 3:
                parameter = JSON.stringify({ cmd: "mcne", args: arg.match(/(\d+)/ig) });
                break;
            case 4:
                parameter = JSON.stringify({ cmd: "h", args: arg });
                break;
            case 5:
                parameter = JSON.stringify({ cmd: "p" });
                break;
            case 6:
                parameter = JSON.stringify({ cmd: "sp" });
                break;
            case 7:
                parameter = JSON.stringify({ cmd: "dp" });
                break;
            case 8:
                parameter = JSON.stringify({ cmd: "r" });
                break;
            case 9:
                parameter = JSON.stringify({ cmd: "sr" });
                break;
            case 10:
                parameter = JSON.stringify({ cmd: "s" });
                break;

        }
        $http.post("./speakerphone", parameter).success(function (data, status, headers, config) {
            $scope.resp = data.stringify();
        }).error(function (data, status, headers, config) {
            $scope.resp = data.stringify();
        });
    };
});

app.controller('Auth', function ($scope, $http, $window) {
    $scope.login = '101';
    $scope.pass = '1234';
    $scope.address = '192.168.10.83';
    $scope.error = false;
    $scope.loginSend = function () {
        if ($scope.login != "" && $scope.pass != "" && $scope.address != "") {
            $http.post('/register', { login: $scope.login, pass: $scope.pass, ip: $scope.address }).success(function (response) {
                if (response == 'OK') {
                    $window.location.href = '/logic.html';
                }
                if (response == 'ERR') {
                    $scope.error = true;
                }
            });
        } else {
            alert('Заполните все поля!');
        }
    };
});